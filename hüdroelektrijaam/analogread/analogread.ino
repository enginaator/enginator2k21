
const int analogPin = A0;
double outputValue = 0;
short sensorValue = 0;
unsigned long tims = millis();


void setup() {
    Serial.begin(115200);
    pinMode(analogPin, INPUT);
    //Serial.println(fabs(12.86 * 3.6 * 1.05));
    
}

void loop() {
    // YOUR CODE GOES HERE
    sensorValue = analogRead(analogPin);  
    //outputValue = ((double)sensorValue) / 1024.0 * 5.0; // see pinge mis jõudis pinnnini
    //outputValue = fabs(outputValue * 3.6 * 1.05);

    //Serial.println(outputValue);
    //Serial.write(sensorValue);
    printJSON(sensorValue, millis());
    delay(50);
}

void printJSON(int sensorValue, unsigned long curTime){
  Serial.print("{\"sensorValue\":");
  Serial.print(sensorValue);
  Serial.print(",\"time\":");
  Serial.print(curTime);
  Serial.println("}");
}
