import serial

class serialCommunicator:
    
    def __init__(self):
        self.ser = serial.Serial(port="COM3", baudrate=115200, timeout=5)
        for _ in range(3):
            self.ser.read()
    

    def read(self):
        while self.ser.in_waiting:
            self.last_read = self.ser.readline().decode().strip()
        return self.last_read