import pyqtgraph as pg 
from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtGui import QFont
import numpy as np
import time
from random import random

voltageData = []
voltageCurve = None
voltagePlot = None

currentData = []
currentCurve = None
currentPlot = None

powerData = []
powerCurve = None
powerPlot = None

energyData = []
energyCurve = None
energyPlot = None

timeData = []

newMes = None
WINDOW_SIZE = 30

labels = []

runningAverage = { "U" : 0, "I": 0, "P": 0 }

def update():
    global voltageData, voltageCurve, voltagePlot, currentData, currentCurve, currentPlot, powerData, powerCurve, powerPlot, timeData, newMes, energyData, energyCurve, energyPlot, labels, runningAverage
    
    measurment = newMes()

    if measurment == None:
        return

    voltageData.append(measurment["U"])
    currentData.append(measurment["I"])
    timeData.append(measurment["t"])
    powerData.append(measurment["P"])
    if len(energyData) > 0:
        energyData.append(energyData[-1] + powerData[-1] * (timeData[-1] - timeData[-2]))
    else:
        energyData.append(0)
    
    voltageCurve.setData(np.array([timeData, voltageData]).transpose())
    currentCurve.setData(np.array([timeData, currentData]).transpose())
    powerCurve.setData(np.array([timeData, powerData]).transpose())
    energyCurve.setData(np.array([timeData, energyData]).transpose())

    if measurment["U"] != 0:
        if timeData[-1] - timeData[0] > WINDOW_SIZE:
            voltagePlot.setXRange(timeData[-1] - WINDOW_SIZE, timeData[-1])
            currentPlot.setXRange(timeData[-1] - WINDOW_SIZE, timeData[-1])
            powerPlot.setXRange(timeData[-1] - WINDOW_SIZE, timeData[-1])
            energyPlot.setXRange(timeData[-1] - WINDOW_SIZE, timeData[-1])
        else:
            voltagePlot.setXRange(timeData[0], timeData[-1])
            currentPlot.setXRange(timeData[0], timeData[-1])
            powerPlot.setXRange(timeData[0], timeData[-1])
            energyPlot.setXRange(timeData[0], timeData[-1])
    
        size = len(np.nonzero(voltageData)[0])
        runningAverage["U"] = (runningAverage["U"] * (size - 1) + voltageData[-1]) / size
        runningAverage["I"] = (runningAverage["I"] * (size - 1) + currentData[-1]) / size
        runningAverage["P"] = (runningAverage["P"] * (size - 1) + powerData[-1]) / size
    
    
    labels[0].setText(f"Pinge keskmine: {round(runningAverage['U'], 3)}V")
    labels[1].setText(f"Voolutugevuse keskmine: {round(runningAverage['I'] * 1000, 3)}mA")
    labels[2].setText(f"Võimsuse keskmine: {round(runningAverage['P'] * 1000, 3)}mW")
    labels[3].setText(f"Koguenergia: {round(energyData[-1] * 1000, 3)}mJ")
    

def start(getNewMeasurement):
    global newMes, voltageCurve, voltagePlot, currentPlot, currentCurve, powerPlot, powerCurve, energyPlot, energyCurve, labels

    font=QtGui.QFont()
    font.setPixelSize(25)

    newMes = getNewMeasurement
    app = QtGui.QApplication([])
    win = pg.GraphicsLayoutWidget(show=True, title="Plot")
    win.resize(1000,600)
    win.setWindowTitle('Hüdroelektrijaam')
    pg.setConfigOptions(antialias=True)

    voltagePlot = win.addPlot(title="Pinge")
    voltageCurve = voltagePlot.plot()
    voltagePlot.setLabel("bottom", "aeg", "s")
    voltagePlot.setLabel("left", '<div style="font-size:25px;">Pinge</div>', "V")
    voltagePlot.getAxis("bottom").setStyle(tickFont = font)
    voltagePlot.getAxis("left").setStyle(tickFont = font)

    win.nextRow()  ## kommenteeri mind välja kui tahad 2x2

    powerPlot = win.addPlot(title="Võimsus")
    powerCurve = powerPlot.plot()
    powerPlot.setLabel("bottom", "aeg", "s")
    powerPlot.setLabel("left", '<div style="font-size:25px;">Võimsus</div', "W")
    powerPlot.getAxis("bottom").setStyle(tickFont = font)
    powerPlot.getAxis("left").setStyle(tickFont = font)

    win.nextRow()

    currentPlot = win.addPlot(title="Voolutugevus")
    currentCurve = currentPlot.plot(pen=pg.mkPen('w'))
    currentPlot.setLabel("bottom", "aeg", "s")
    currentPlot.setLabel("left", '<div style="font-size:25px;">Voolutugevus</div>', "A")
    currentPlot.getAxis("bottom").setStyle(tickFont = font)
    currentPlot.getAxis("left").setStyle(tickFont = font)

    win.nextRow() ## kommenteeri mind välja kui tahad 2x2

    energyPlot = win.addPlot(title="Energia")
    energyCurve = energyPlot.plot()
    energyPlot.setLabel("bottom", "aeg", "s")
    energyPlot.setLabel("left", '<div style="font-size:25px;">Energia</div>', 'J')
    energyPlot.getAxis("bottom").setStyle(tickFont = font)
    energyPlot.getAxis("left").setStyle(tickFont = font)

    voltagePlot.setYRange(0, 4)
    currentPlot.setYRange(0, 0.025)
    powerPlot.setYRange(0, 0.1)
    # energyPlot.setYRange(__, __)

    timer = QtCore.QTimer()
    timer.timeout.connect(update)
    timer.start(50)

    app2 = QWidget()
    
    for i in range(4):
        labels.append(QLabel(app2))
        labels[-1].setFont(QFont('Arial', 30))
        labels[-1].setGeometry(0, i * 100, 900, 100)
    
    
    app2.show()
    QtGui.QApplication.instance().exec_()
    

def getNewMeasurement():
    return { "U": 3+random(), "I": 0.05+random()/100, "P": 0.04+random()/10, "t": time.time() % 3600 }


if __name__ == "__main__":
    start(getNewMeasurement)