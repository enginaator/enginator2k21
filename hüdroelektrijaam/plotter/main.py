import json
import requests

from serialCommunicator import serialCommunicator
import Graph

com = None
f = None

def getNewMeasurement():
    global com, f
    try:
        vastusJson = json.loads(com.read())
        if vastusJson != None and "sensorValue" in vastusJson and "time" in vastusJson:
            newJson = convert(vastusJson)
        else: 
            raise Exception("Bad json from arduino")
        print(newJson)
        f.write(json.dumps(newJson))
        f.write("\n")
        # requests.post('http://localhost:8000/data', data = newJson)
        return newJson
    except Exception as e:
        print(type(e))
        print(e)
        return None


def convert(data):
    newData = {} 
    newData["U"] = data["sensorValue"]  / 1024.0 * 5.0 * (120/47 + 1) #* 1.02
    newData["I"] = newData["U"] / (120+47)
    newData["P"] = newData["U"] * newData["I"]
    newData["t"] = data["time"] / 1000
    return newData
#120
#47
# 11.12J

def main():
    global com, f
    import restart  # clears database
    com = serialCommunicator()
    f = open("loppPumbaga.txt", "a+")
    for i in range(50):
        com.read()
    
    Graph.start(getNewMeasurement)
    com.ser.close()


if __name__ == "__main__":
    main()