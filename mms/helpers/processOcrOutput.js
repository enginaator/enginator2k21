module.exports = (data) => {
    console.log("processOrcOutput pikkus: " + data.analyzeResult.readResults[0].lines.length)
    var currentMax = 0;
    var currentText = -1;

    for(let index = 0; index < data.analyzeResult.readResults[0].lines.length; index++) {

        const line = data.analyzeResult.readResults[0].lines[index];

        lineSize = (line.boundingBox[4]-line.boundingBox[0]) * (line.boundingBox[5]-line.boundingBox[1])

        rida = parseFloat(line.text.replace(/\s/g, ''));
        console.log(rida)
        if(!isNaN(rida)){
            if (Math.abs(lineSize) > currentMax) {
                currentMax = Math.abs(lineSize);
                currentText = rida;
            }
        }

    }

    console.log("Current text is" + currentText);
    return parseFloat(currentText);
}