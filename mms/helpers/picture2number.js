const axios = require("axios");
const DbController = require("../dblogic/dbController");
const processOcrOutput = require("./processOcrOutput");

const controller = new DbController()

async function picture2number (body, timestamp, userId) {
    let resp;
    if (Buffer.isBuffer(body)) {
        resp = await axios.post(process.env.OCR_URL + "/read/analyze", body,
        { headers: {
            "Content-Type": "application/octet-stream",
            "ocp-apim-subscription-key": process.env.OCR_KEY
        }})
    } else {
        resp = await axios.post(process.env.OCR_URL + "/read/analyze", body, 
        { headers: {
            "ocp-apim-subscription-key": process.env.OCR_KEY
        }})
    }
    
    let res2;

    while (true) {
        res2 = await axios.get(process.env.OCR_URL + "/read/analyzeResults/" + resp.headers["apim-request-id"], {
            headers: {
                "ocp-apim-subscription-key": process.env.OCR_KEY
            }
        })
    
        if (res2.data.status == "succeeded") {
            break
        } else {
            await new Promise(resolve => setTimeout(resolve, 100))
        }
    }

    let number = processOcrOutput(res2.data)
    
    await controller.insert(userId, timestamp, number)
    
    //return number
    
}

module.exports = picture2number