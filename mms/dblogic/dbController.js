const mongoose = require('mongoose');
const Monitoring = require('./models/monitoring');
const Alert = require('./models/alert');

mongoose.connect(process.env.DB_URI);

const monitorings = mongoose.model('monitorings', Monitoring);
const alerts = mongoose.model('alerts', Alert);

module.exports = class DbController {
    async insert(userId, timestamp, measurement) {
        const lastEntry = await monitorings
            .findOne({ userId }, {}, { sort: { timestamp: -1 } })
            .exec(); // vb ei anna viimast

        console.log(`last entry: ${lastEntry}`);

        if (!lastEntry) {
            console.log('no previous records');
            const newMonitoring = new monitorings({
                userId,
                measurement,
                delta: 0,
                timestamp,
            });
            if (measurement < 0){
                await new alerts({
                    userId,
                    message: 'Measurement is negative. Monitoring saving cancelled.',
                    timestamp,
                    newMonitoring,
                }).save();
                return;
            }
            console.log(newMonitoring);
            await newMonitoring.save();
            return;
        }
        console.log(
            `inserting: ${
                userId
            } ${
                timestamp
            } ${
                measurement
            } ${
                measurement - lastEntry.measurement}`,
        );
        const delta = measurement - lastEntry.measurement;
        const newMonitoring = new monitorings({
            userId,
            measurement,
            delta,
            timestamp,
        });
        if (delta >= 0) {
            console.log(newMonitoring);
            if (measurement < 0){
                await new alerts({
                    userId,
                    message: 'Measurement is negative. Monitoring saving cancelled.',
                    timestamp,
                    newMonitoring,
                }).save();
                return;
            }
            await newMonitoring.save();
            this.checkAlert(userId, timestamp);
        } else {
            console.log('Delta is negative or nan. Monitoring saving cancelled.');
            await new alerts({
                userId,
                message: 'Delta is negative or nan. Monitoring saving cancelled.',
                timestamp,
                newMonitoring,
            }).save();
            throw Error('Delta is negative or nan. Monitoring saving cancelled.')
        }
    }

    async checkAlert(userId, timestamp) {
        let result = await monitorings
            .find({
                userId,
                timestamp: { $gte: timestamp - 24 * 60 * 60 * 1000 },
            })
            .exec();
        if (result.length == 0) {
            console.log('No monitorings found in last 24 hours');
        } else {
            console.log(result);
            console.log(result.length);
            let resultFiltered = result.filter((monitoring) => monitoring.delta == 0);
            console.log(
                `Filtered monitorings with delta=0 in last 24 hours${resultFiltered.length}`,
            );

            if (resultFiltered.length == 0) {
                console.log(`Alert: No delta=0 in last 24 hours, userid=${userId}`);
                await new alerts({
                    userId,
                    message: 'Possible waterleak',
                    timestamp,
                    monitoringObj: result[result.length - 1],
                }).save();
            }
        }
    }
};
