const mongoose = require('mongoose');
const Monitoring = require('./monitoring');

module.exports = mongoose.Schema({
    userId: { type: String, required: true },
    message: { type: String, required: true },
    timestamp: { type: Date, required: true },
    monitoringObj: { type: Monitoring }
});
