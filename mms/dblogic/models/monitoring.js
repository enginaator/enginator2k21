const mongoose = require('mongoose');

const MonitoringSchema = mongoose.Schema({
    userId: { type: String, required: true },
    measurement: { type: Number, required: true },
    delta: { type: Number, required: true },
    timestamp: { type: Date, required: true },
});
MonitoringSchema.index({userId: 1});

module.exports = MonitoringSchema
