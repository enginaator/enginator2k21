function onFileSelected(evt) {
    const fileInput = evt.target;
    const files = Array.from(evt.target.files).map(file => file.name);
    const placeholder = fileInput.getAttribute('data-placeholder') || fileInput.placeholder;
    // Store original placeholder
    fileInput.setAttribute('data-placeholder', placeholder);
    // Update current placeholder
    if (files.length === 1) {
        fileInput.placeholder = files[0].replace(/.*[\/\\]/, '');
    }
    else if (files.length > 1) {
        fileInput.placeholder = `${files.length} files`;
    }
    else {
        fileInput.placeholder = placeholder;
    }
}

const el = document.getElementById("fileInput")
if (el) {
    el.addEventListener('change', onFileSelected)
}

async function onSubmit() {
    const urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
    const url = document.getElementById("url").value
    const userId = document.getElementById("userId").value
    const file = document.getElementById("fileInput").files[0]
    let error = false
    if (!userId) {
        document.getElementById("userIdError").innerHTML = 'UserId is required'
        error = true
    } else {
        document.getElementById("userIdError").innerHTML = ''
    }
    
    if ((file && url) || (!file && !url) ){
        document.getElementById("submitError").innerHTML = 'Please choose one: url or file'
        error = true
    } else {
        document.getElementById("submitError").innerHTML = ''
    }
    
    if (url && !url.match(urlRegex) && !file) {
        document.getElementById("pictureUrlError").innerHTML = 'Url is in wrong format'
        error = true
    } else {
        document.getElementById("pictureUrlError").innerHTML = ''
    }
    
    if (error) {
        return;
    }
    if (url){
        let res = await fetch("/api/camera", {
            method: "POST",
            body: {
                url: url
            },
            headers: {
                userId: userId
            }
        })
        console.log(res)
    }
    else if (file){
        await readImage(file, userId)
    }
}

async function readImage(file, userId) {
    // Check if the file is an image.
    if (file.type && !file.type.startsWith('image/')) {
      console.log('File is not an image.', file.type, file);
      document.getElementById("fileError").value = ('File is not an image.' + file.type + ' ' + file)
      return;
    }
  
    const reader = new FileReader();
    reader.addEventListener('load', async (event) => {
      file.src = event.target.result;
      // Base64 string sisse tulnud failist. 
      console.log(file.src)
      let blob = new Blob([file.src], {type: `${file.type}`})
      let res = await fetch("/api/camera", {
          method: "POST",
          body: blob,
          headers: {
            userId: userId
        }
      })
      console.log(res)
    });
    reader.readAsDataURL(file);
}