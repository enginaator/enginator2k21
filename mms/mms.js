require('dotenv').config();

const express = require("express")
const bodyParser = require('body-parser');
const lowercaseKeys = require('./helpers/lowercaseKeys');
const picture2number = require("./helpers/picture2number")

app = express()

app.use(bodyParser.json({}))
app.use(bodyParser.raw({ limit: '1000kb', type: '*/*' }));
app.use(express.static('front-end'))

app.post("/api/camera", async (req, res) => {
    let timestamp = Date.now()
    let userId = req.headers.userid

    try {
        if (Buffer.isBuffer(req.body)) {
            res.send(await picture2number(req.body, timestamp, userId))
        } else if (lowercaseKeys(req.body).url) {
            res.send(await picture2number({url: lowercaseKeys(req.body).url}, timestamp, userId))
        } else {
            res.status(400).json({ "error": { message: "Faulty data" }})
        }
    } catch (e) {
        if(e.response){
            console.log(e.response)
            res.status(500).json({ "error": { message: "Server error", res: e.response }})
        } else {
            res.status(400).json({ "error": { message: `Bad request: ${e.message}` }})
        }
    }
})



app.listen(process.env.PORT || 4000, () => console.log("server listening on 4000"))