const analyse = require('../handlers/analyse');
const areaOfInterest = require('../handlers/areaOfInterest');
const {
    analyzePut,
    analyzeGet,
    analyzeStatusGet,
} = require('../handlers/batch');
const describe = require('../handlers/describe');
const detect = require('../handlers/detect');
const generateThumbnail = require('../handlers/generateThumbnail');
const { modelsGet, modelsAnalyse } = require('../handlers/models');
const ocr = require('../handlers/ocr');
const { analyzeResults, analyze } = require('../handlers/read');
const tag = require('../handlers/tag');

const NAME_ID = {
    analyze: {
        POST: '56f91f2e778daf14a499f21b',
    },
    describe: {
        POST: '56f91f2e778daf14a499f21f',
    },
    detect: {
        POST: '5e0cdeda77a84fcd9a6d4e1b',
    },
    areaOfInterest: {
        POST: 'b156d0f5e11e492d9f64418d',
    },
    read: {
        analyzeResults: {
            GET: '5d9869604be85dee480c8750',
        },
        analyze: {
            POST: '5d986960601faab4bf452005',
        },
    },
    generateThumbnail: {
        POST: '56f91f2e778daf14a499f20c',
    },
    models: {
        GET: '56f91f2e778daf14a499f20e',
        analyze: {
            POST: '56f91f2e778daf14a499f311',
        },
    },
    ocr: {
        POST: '56f91f2e778daf14a499f20d',
    },
    tag: {
        POST: '56f91f2e778daf14a499f200',
    },
    batch: {
        analyze: {
            PUT: '6255e4f0fe1a47d79b577145',
            GET: '165c19b6a1574d4c8971af47',
        },
        analyzeStatus: {
            GET: '650d21697bf6473aa7011a06',
        },
    },
};

module.exports = {
    [NAME_ID.analyze.POST]: analyse,
    [NAME_ID.describe.POST]: describe,
    [NAME_ID.detect.POST]: detect,
    [NAME_ID.areaOfInterest.POST]: areaOfInterest,
    [NAME_ID.read.analyzeResults.GET]: analyzeResults,
    [NAME_ID.generateThumbnail.POST]: generateThumbnail,
    [NAME_ID.models.GET]: modelsGet,
    [NAME_ID.ocr.POST]: ocr,
    [NAME_ID.read.analyze.POST]: analyze,
    [NAME_ID.models.analyze.POST]: modelsAnalyse,
    [NAME_ID.tag.POST]: tag,
    [NAME_ID.batch.analyze.PUT]: analyzePut,
    [NAME_ID.batch.analyze.GET]: analyzeGet,
    [NAME_ID.batch.analyzeStatus.GET]: analyzeStatusGet,
};
