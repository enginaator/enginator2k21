const ocrjson = require('../jsons/ocr.json');
const lowercaseKeys = require('../helpers/lowercaseKeys');

module.exports = (req, res) => {
    if (Buffer.isBuffer(req.body)) {
        res.json(ocrjson);
    } else {
        req.body = lowercaseKeys(req.body);
        const ocrjsoncopy = JSON.parse(JSON.stringify(ocrjson));
        ocrjsoncopy.regions = [ocrjsoncopy.regions[0]];
        ocrjsoncopy.regions[0].lines = [ocrjsoncopy.regions[0].lines[0]];
        ocrjsoncopy.regions[0].lines[0].words = [
            ocrjsoncopy.regions[0].lines[0].words[0],
        ];
        ocrjsoncopy.regions[0].lines[0].words[0].text = req.body.url;
        res.json(ocrjsoncopy);
    }
};
