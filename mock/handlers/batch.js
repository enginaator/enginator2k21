module.exports = {
    analyzeGet: (req, res) => {
        res.send('batch.js analyzeGet');
    },
    analyzePut: (req, res) => {
        res.send('batch.js analyzePut');
    },
    analyzeStatusGet: (req, res) => {
        res.send('batch.js analyzeStatusGet');
    },
};
