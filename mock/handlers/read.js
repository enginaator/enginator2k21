const readAnalyzeResults = require('../jsons/readAnalyzeResults.json');
const lowercaseKeys = require('../helpers/lowercaseKeys');
const urlEncode = require('../helpers/urlEncoder');

module.exports = {
    analyzeResults: (req, res) => {
        const opId = req.params.operationId;
        const response = readAnalyzeResults;
        for (
            let i = 0;
            i < response.analyzeResult.readResults[0].lines.length;
            // eslint-disable-next-line no-plusplus
            i++
        ) {
            response.analyzeResult.readResults[0].lines[i].text = opId.toString();
            response.analyzeResult.readResults[0].lines[i].words = [
                response.analyzeResult.readResults[0].lines[i].words[0],
            ];
            response.analyzeResult.readResults[0].lines[i].words[0].text = opId.toString();
        }
        res.json(response);
    },

    analyze: (req, res) => {
        if (Buffer.isBuffer(req.body)) {
            res.set({
                'Operation-Location': `${req.get('host') + req.url}Results/69420`,
                'apim-request-id': `69420`,
            });
            res.send();
        } else if (lowercaseKeys(req.body).url) {
            res.set({
                'Operation-Location': `${req.get('host') + req.url}Results/${urlEncode(lowercaseKeys(req.body).url)}`,
                'apim-request-id': `${urlEncode(lowercaseKeys(req.body).url)}`,
            });
            res.send();
        } else {
            res.status(400).json({
                error: {
                    code: 'BadArgument',
                    message: 'Invalid input.',
                },
            });
        }
    },
};
