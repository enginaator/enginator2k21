const bodyParser = require('body-parser');
const express = require('express');
const { initialize } = require('express-openapi');
const openApi = require('./openapi.json');
const operations = require('./helpers/operations');

const app = express();

app.use((req, res, next) => {
    if (req.headers["ocp-apim-subscription-key"]) next()
    else res.status(401).json({
        "error": {
            "code": "401",
            "message": "Access denied due to invalid subscription key or wrong API endpoint. Make sure to provide a valid key for an active subscription and use a correct regional API endpoint for your resource."
        }
    })    
})
app.use(bodyParser.json({}));
app.use(bodyParser.raw({ limit: '1000kb', type: '*/*' }));

initialize({
    app,
    apiDoc: openApi,
    operations,
    errorMiddleware: (err, req, res, next) => {
        console.log(err);
        next();
    },
});

app.listen(process.env.PORT || 3000, () => console.log('server listening on port 3000'));
